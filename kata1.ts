export function Add(numbers: string): number | string {
    let delimiter: string|RegExp = /,|\n/;
    let finalNumbers = numbers;
    if (numbers.startsWith('//')){
        delimiter = numbers.substring(2, numbers.indexOf('\n'));
        finalNumbers = numbers.substring(numbers.indexOf('\n') + 1);
    }
    let numArray = finalNumbers.split(delimiter);
    let sum = 0;
    const negNumArray = [];
    for (let index = 0; index < numArray.length; index++) {
        const num = Number(numArray[index]);
        num < 0 ?  negNumArray.push(num) : sum += num;
    }
    if (negNumArray.length > 0)
        return 'Negatives not allowed [' + negNumArray + ']';
    return sum;
}