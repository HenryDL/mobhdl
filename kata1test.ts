import { Add } from "./kata1";

export function runTests(){
    testAdd(0, '');
    testAdd(1, '1');
    testAdd(3, '1,2');
    testAdd(6, '1,2\n3');
    testAdd(3, '//;\n1;2');
    testAdd('Negatives not allowed [-1]', '1, -1');
    testAdd('Negatives not allowed [-1,-2,-1]', '1, -1, -2, -1, 4');
}

function testAdd(expected: number | string, numbers: string) {
    console.log('Expected: ', expected, ', Actual:', Add(numbers));
}