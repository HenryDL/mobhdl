# MobHDL
---
## LINKS
### Mob-sh: [https://mob.sh/](https://mob.sh/)
### GitLab: [https://gitlab.com/HenryDL/mobhdl/-/tree/mob/main](https://gitlab.com/HenryDL/mobhdl/-/tree/mob/main)
### Kata: [https://kata-log.rocks/string-calculator-kata](https://kata-log.rocks/string-calculator-kata)

---
## SETUP
1. Setup mob-sh (if not already present on your PC)
2. Install VSCode (if not already present on your PC)
3. (Clone the repository from GitLab and open it in VSCode)
   - Open the Terminal view in VSCode (by pressing _CTRL+`_)
4. Install NodeJS (if not already present on your PC)
   - Recommended to use nvm-windows (makes it easier to switch between versions)
     - https://github.com/coreybutler/nvm-windows/releases/tag/1.1.7
   - Steps:
     1. (Restart VCCode if it was open after installing nvm)
     2. `nvm install 16.5.0`
     3. `nvm list`
     4. `nvm use 16.5.0`
     5. Check NPM is installed: `npm -v`
5. Install Typescript and ts-node in the project
   - To do this, from the VSCode Terminal run: `npm install`
4. In VSCode under the _EXPLORER_ menu, expand the _NPM SCRIPTS_ accordion (if you don't see it check under the _..._ icon i n the top-right)

---
## RUN
- To run the steps:
   - Execute (press the play icon) next to the _RUN-STEPS_ action (which should be listed under _NPM SCRIPTS_)
   - Or type `npm run ts-node` in the _TERMINAL_ (which should be pointing to the root folder of the cloned repository)
- To do the Mob steps:
   - `mob next`
      - Run this when you have finished being the typist
      - This will commit your changes and push the commit up to the GitLab repository
   - `mob start`
      - Run this when you are starting off as the next typist
      - This will pull the latest code from the GitLab repository

